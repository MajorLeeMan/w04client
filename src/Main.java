//CLIENT
/*OBJECTIVES:
a:Write an application with two separate components.  The first is an HTTP server
component that when called takes an object and converts it to JSON using the Jackson
JSON parser, and returns a block of JSON using HTTP.  The second component is a client
component.  It makes a call to the server component, gets the JSON it returns, and then
converts that using the Jackson JSON parser, into an object.

b:These two components need to be written as two separate classes.

c:Make sure you incorporate error handling and data validation into your programs,
including catching the most specific type of exceptions you can.
*/
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;




public class Main {


    public static void main(String[] args) {

        System.out.println("Hello. Starting...");
        //create a new connection as client
        HttpClient client = HttpClient.newHttpClient();
        //create a request and send it to the target server on the local host
        HttpRequest request = HttpRequest.newBuilder().uri(URI.create("http://localhost:0001")).build();
        System.out.println("Connecting... failure in 5 seconds or less.");
        client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                .thenApply(HttpResponse::body)
                .thenAccept(System.out::println)
                .join();
        //if successful, print the data received
        System.out.println("Data received.");
        System.out.println(client);
    }

}



/*
public class HTTPExample {
    public static String getHttpContent(String string) {

        String content = "";

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader();
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            content = stringBuilder.toString();

        } catch (Exception e) {
            System.err.println(e.toString());
        }
        return content;
    }
*/
/*
public class Main {


    public static String customerToJSON(Customer customer){
        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(customer);
        } catch (JsonProcessingException error) {
            System.err.println(error.toString());
        }
        return s;
    }

    public static Customer JSONToCustomer(String s) {
        ObjectMapper mapper = new ObjectMapper();
        Customer customer = null;

        try {
            customer = mapper.readValue(s, Customer.class);
        } catch (JsonProcessingException error) {
            System.err.println(error.toString());
        }
        return customer;
    }

    public static void main(String[] args) {
        Customer cust = new Customer("Troy", 12345);
        cust.setName("Troy");
        cust.setPhone(12345);
        String json = Main.customerToJSON(cust);
        System.out.println(json);
        Customer cust2 = Main.JSONToCustomer(json);
        System.out.println(cust2);
    }
}
*/

